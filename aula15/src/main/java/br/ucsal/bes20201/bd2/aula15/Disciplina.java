package br.ucsal.bes20201.bd2.aula15;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_disciplina")
public class Disciplina {

	@Id
	private String codigo;

	private String nome;

	public Disciplina() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Disciplina(String codigo, String nome) {
		super();
		this.codigo = codigo;
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
