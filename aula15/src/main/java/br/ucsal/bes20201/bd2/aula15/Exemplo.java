package br.ucsal.bes20201.bd2.aula15;

import java.util.ArrayList;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		EntityManager em = emf.createEntityManager();

		// Criar um toString em Cidade (atenção com o atributo estado) e em Estado;

		// popularBa(em);

		// Limpar o cache do EntityManager
		em.clear();

		// Buscar a cidade que foi cadastrada anteriormente
		// Cidade cidade1 = em.find(Cidade.class, "SSA");
		// System.out.println("Cidade1=" + cidade1);
		// System.out.println("Cidade1=" + cidade1.getSigla() + "-" +
		// cidade1.getNome());

		// Buscar a estado que foi cadastrado anteriormente
		em.getTransaction().begin();
		Estado estadoSP = new Estado("SP", "São Paulo", new ArrayList<>());
		Cidade cidadeSAO = new Cidade("SAO", "São Paulo", estadoSP);
		estadoSP.getCidades().add(cidadeSAO);
		em.persist(estadoSP);
		em.getTransaction().commit();

		em.clear();

		Estado estado1 = em.find(Estado.class, "SP");
		System.out.println(estado1.getCidades());

		em.getTransaction().begin();
		em.remove(estado1);
		em.getTransaction().commit();

		em.close();
		emf.close();

	}

	private static void popularBa(EntityManager em) {
		// Popular a base com estado e cidade
		em.getTransaction().begin();
		Estado estadoBA = new Estado("BA", "Bahia", null);
		em.persist(estadoBA);
		Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
		Cidade cidadeFDS = new Cidade("FDS", "Feira de Santana", estadoBA);
		em.persist(cidadeSSA);
		em.persist(cidadeFDS);
		em.getTransaction().commit();
	}

}
