package br.ucsal.bes20201.bd2.aula17.exemplo;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.ucsal.bes20201.bd2.aula17.domain.Cidade;
import br.ucsal.bes20201.bd2.aula17.domain.Estado;

public class ExemploEstadoCidade {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		Estado estadoSP = new Estado("SP", "São Paulo", new ArrayList<>());
		Cidade cidadeSAO = new Cidade("SAO", "São Paulo", estadoSP);
		Cidade cidadeGRU = new Cidade("GRU", "Guarulhos", estadoSP);
		estadoSP.getCidades().add(cidadeSAO);
		estadoSP.getCidades().add(cidadeGRU);
		em.persist(estadoSP);
		em.getTransaction().commit();

		em.getTransaction().begin();
		estadoSP.getCidades().remove(1);
		em.getTransaction().commit();

		em.clear();

		estadoSP = em.find(Estado.class, "SP");
		System.out.println(estadoSP.getCidades());

		em.close();
		emf.close();

	}

}
