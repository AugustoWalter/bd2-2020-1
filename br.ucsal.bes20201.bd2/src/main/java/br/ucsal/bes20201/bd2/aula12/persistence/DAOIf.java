package br.ucsal.bes20201.bd2.aula12.persistence;

import java.sql.SQLException;
import java.util.List;

public interface DAOIf<T> {

	public T findById(Integer id) throws SQLException;

	public List<T> findAll() throws SQLException;

	public T insert(T entity) throws SQLException;

	public void update(T entity) throws SQLException;

	public void delete(T entity) throws SQLException;

	public void delete(Integer id) throws SQLException;
}
