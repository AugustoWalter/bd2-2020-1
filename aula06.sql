-- Alterar a tabela de acessórios para incluir um atributo tipo acessório: 
-- nome: tp;
-- datatype: char(1);
-- opcional (inicialmente, pois já devem existir acessórios cadastrados).
-- valores válidos: F (instalados na fábrica), C (instalados na concessionária), L (instalados em lojas);
-- popular a tabela acessorio com dados de tipo de acessorio para todos os acessórios já cadastrados;
-- alterar o atributo tp para que o mesmo seja obrigatório;
insert into acessorio (sg, nm)
values 
('ar','Ar condicionado'),
('dh','Direção hidráulica'),
('mm','Multimidia'),
('ve','Vidro elétrico'),
('te','Trava elétrica');

select *
from acessorio;

-- SOLUÇÃO 1:
-- Adicionou a coluna tp permitindo valores nulos;
-- Alterou os dados, definindo valores para os registros existentes;
-- Alteramos a coluna para que a mesma não permitisse valores nulos;
alter table acessorio
add column tp char(1) check (tp in ('F', 'C', 'L'));
-- add column tp char(1) check (tp = 'F' or tp = 'C' or  tp = 'L');

select *
from acessorio;

update acessorio
set tp = 'F'
where sg in ('ar','dh');
-- where sg = 'ar' or sg = 'dh';

update acessorio
set tp = 'C'
where sg in ('ve','te');

update acessorio
set tp = 'L'
where sg = 'mm';

select *
from acessorio;

alter table acessorio
alter column tp set not null;

-- SOLUÇÃO 2:
-- Adicionou a coluna tp não permitindo valores nulos, todavia fornecendo um valor default;
-- Ajustar os registros que não estavam adequados para o default proposto;
-- Opcionalmente, podemos remover esse valor default após a criação da coluna;
delete 
from acessorio;

alter table acessorio 
drop column tp;

insert into acessorio (sg, nm)
values 
('ar','Ar condicionado'),
('dh','Direção hidráulica'),
('mm','Multimidia'),
('ve','Vidro elétrico'),
('te','Trava elétrica');

select * 
from acessorio;

alter table acessorio
add column tp char(1) not null check (tp in ('F', 'C', 'L')) default 'F';

select * 
from acessorio;

update acessorio
set tp = 'C'
where sg in ('ve','te');

update acessorio
set tp = 'L'
where sg = 'mm';

select * 
from acessorio;

alter table acessorio
alter column tp drop default;



-- Listar as placas e nome dos modelo de veículos que possuem ao menos um acessório de fábrica, 
-- ordernado por nome de modelo e placa;
select *
from veiculo;

insert into veiculo_acessorio (nu_placa_veiculo, sg_acessorio)
values 
('ABC1234','ar'),
('CDE4567','mm'),
('CDE4567','ve');

--NÃO FAÇA ASSIM!!!! O a.tp = 'F' NÃO é um CRITÉRIO de junção!!!
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join veiculo_acessorio va on (va.nu_placa_veiculo = v.nu_placa)
inner join acessorio a on (a.sg = va.sg_acessorio and a.tp = 'F')
order by m.nm asc,
	 v.nu_placa asc;

--Pode não ser interessante fazer assim. Talvez não fique tão claro seu objetivo na consulta.
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join veiculo_acessorio va on (va.nu_placa_veiculo = v.nu_placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where  a.tp = 'F'
order by m.nm asc,
	 v.nu_placa asc;

-- Assim fica mais claro!
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join veiculo_acessorio va on (va.nu_placa_veiculo = v.nu_placa)
where  va.sg_acessorio in (   select a.sg
				from acessorio a
				where a.tp = 'F')
order by m.nm asc,
	 v.nu_placa asc;

-- Essa subconsulta PODE ter um problema GRAVE performance, pois cada registro da consulta externa gera uma execução da consulta interna.
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join veiculo_acessorio va on (va.nu_placa_veiculo = v.nu_placa)
where  exists (   select 1
				from acessorio a
				where a.sg = va.sg_acessorio
				and   a.tp = 'F')
order by m.nm asc,
	 v.nu_placa asc;




-- Listar as placas e nome dos modelo de veículos que NÃO possuem nenhum acessório, 
-- ordernado por nome de modelo e placa;

--NÃO FAÇA ASSIM!!!! O a.tp = 'F' NÃO é um CRITÉRIO de junção!!!
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
left join veiculo_acessorio va on (va.nu_placa_veiculo = v.nu_placa)
where va.sg_acessorio is null
order by m.nm asc,
	 v.nu_placa asc;

--FAÇA ASSIM. Fica mais claro!
select  m.nm,
	v.nu_placa
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
where v.nu_placa not in (select distinct va.nu_placa_veiculo
			from veiculo_acessorio va)
order by m.nm asc,
	 v.nu_placa asc;



-- Listar as placas dos veículos que possuem algum acessório, ordernado por placa;
select distinct nu_placa_veiculo
from veiculo_acessorio;



-- Criar uma tabela contendo o número da placa e o nome do modelo;

-- Solução 1: criar a tabela e popular a mesma com o resultado de uma consulta;
create table veiculo_modelo_1 (
	nu_placa	char(7) 	not null,
	nm_modelo	varchar(30)	not null);

insert into veiculo_modelo_1 (nu_placa, nm_modelo)
select  v.nu_placa,
	m.nm
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo);	

select *
from veiculo_modelo_1;


-- Solução 2: criar a tabela a partir da consulta;

select  v.nu_placa 	as "nu_placa",
	m.nm		as "nm_modelo"
into 	veiculo_modelo_2
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo);	

select *
from veiculo_modelo_2;



-- Crie um estrutura que permita ao usuário do banco consultar, sem fazer junção, os veiculos, incluindo placa, 
-- vl_ano_fabricacao, vl_km_atual, dt_aquisicao, nome do modelo, nome/cnpj do fabricante do modelo e nome do grupo;
drop view if exists veiculo_detalhado;

create or replace view veiculo_detalhado 
as
	select 	v.nu_placa 			as "placa",  
		v.vl_ano_fabricacao 		as "ano_fabricacao",
		v.vl_km_atual			as "km_atual", 	
		v.dt_aquisicao			as "data_aquisicao",
		m.nm				as "modelo",
		f.nu_cnpj			as "cnpj_fabricante",
		f.nm				as "fabricante",
		coalesce(g.nm,'(sem grupo)')	as "grupo"
	from	veiculo v
	inner join modelo m on (m.cd = v.cd_modelo)
	inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
	left outer join grupo	g on (g.cd = v.cd_grupo);

select *
from veiculo_detalhado vd
inner join veiculo_acessorio va on (va.nu_placa_veiculo = vd.placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where ano_fabricacao = 2010;


create materialized view veiculo_detalhado_materializada
as
	select 	v.nu_placa 			as "placa",  
		v.vl_ano_fabricacao 		as "ano_fabricacao",
		v.vl_km_atual			as "km_atual", 	
		v.dt_aquisicao			as "data_aquisicao",
		m.nm				as "modelo",
		f.nu_cnpj			as "cnpj_fabricante",
		f.nm				as "fabricante",
		coalesce(g.nm,'(sem grupo)')	as "grupo"
	from	veiculo v
	inner join modelo m on (m.cd = v.cd_modelo)
	inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
	left outer join grupo	g on (g.cd = v.cd_grupo);
	

select *
from veiculo_detalhado_materializada vd
inner join veiculo_acessorio va on (va.nu_placa_veiculo = vd.placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where ano_fabricacao = 2010;

update veiculo 
set vl_ano_fabricacao = 2012
where nu_placa = 'ABC1234';

select *
from veiculo_detalhado_materializada vd
inner join veiculo_acessorio va on (va.nu_placa_veiculo = vd.placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where ano_fabricacao = 2010;

select *
from veiculo_detalhado vd
inner join veiculo_acessorio va on (va.nu_placa_veiculo = vd.placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where ano_fabricacao = 2010;

refresh materialized view veiculo_detalhado_materializada;

select *
from veiculo_detalhado_materializada vd
inner join veiculo_acessorio va on (va.nu_placa_veiculo = vd.placa)
inner join acessorio a on (a.sg = va.sg_acessorio)
where ano_fabricacao = 2010;



-- Exclusão de tabelas e outras estruturas

drop table if exists veiculo cascade;
drop table if exists modelo cascade;
drop table if exists grupo cascade;
drop table if exists acessorio cascade;
drop table if exists veiculo_acessorio cascade;
drop table if exists fabricante cascade;
drop table if exists tarifa cascade;
drop type if exists t_endereco cascade;

-- Criação de tabelas com as respectivas chaves primárias, restrições de unicidade e checagem.

create table veiculo (
nu_placa		char(7)		not null,
vl_ano_fabricacao	smallint 	not null,
vl_km_atual		int		not null,
dt_aquisicao		timestamp	not null,
cd_modelo		integer		not null,
cd_grupo		integer		    null,
constraint pk_veiculo
	primary key (nu_placa),
constraint ch_veiculo_ano_fabricacao
	check (vl_ano_fabricacao > 2000));

create table modelo (
cd			serial 		not null,    --int , create sequence, set default
nm			varchar(30)	not null,
nu_cnpj_fabricante	char(14)	not null,
constraint pk_modelo
	primary key (cd),
constraint un_modelo_nm
	unique (nm));

create table grupo (
cd			serial 		not null,
nm			varchar(30)	not null,
constraint pk_grupo
	primary key (cd),
constraint un_grupo_nm
	unique (nm));

create table acessorio (
sg			char(3)		not null,
nm			varchar(30)	not null,
constraint pk_acessorio
	primary key (sg),
constraint un_acessorio_sg
	unique (sg));

create table veiculo_acessorio (
nu_placa_veiculo	char(7)		not null,
sg_acessorio		char(3)		not null,
constraint pk_veiculo_acessorio
	primary key (nu_placa_veiculo,sg_acessorio));

create type t_endereco as (
nm_logradouro		varchar(100),
nu			varchar(10),
nm_bairro		varchar(100),
ds_complemento		varchar(200));
	
create table fabricante (
nu_cnpj			char(14)	not null,
nm			varchar(40)	not null,
nu_telefone		varchar(15)	not null,
endereco		t_endereco	not null,
constraint pk_fabricante
	primary key (nu_cnpj));

create table tarifa (
cd_grupo		int		not null,
dt_inicio_vigencia	date		not null,			
vl			numeric(10,2)	not null,
constraint pk_tarifa
	primary key (cd_grupo, dt_inicio_vigencia));

-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo(cd),
	add constraint fk_veiculo_grupo
		foreign key (cd_grupo)
		references grupo(cd)
		on delete set null;
		
alter table modelo
	add constraint fk_modelo_fabricante
		foreign key (nu_cnpj_fabricante)
		references fabricante(nu_cnpj)
		on update cascade;

alter table veiculo_acessorio 
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (nu_placa_veiculo)
		references veiculo (nu_placa)
		on update cascade
		on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sg_acessorio)
		references acessorio (sg)
		on update cascade;

alter table tarifa
	add constraint fk_tarifa_grupo
		foreign key (cd_grupo)
		references grupo(cd);


