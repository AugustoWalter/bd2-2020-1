﻿-- Exclusão de tabelas e outras estruturas

drop table if exists veiculo cascade;
drop table if exists modelo cascade;
drop table if exists grupo cascade;
drop table if exists acessorio cascade;
drop table if exists veiculo_acessorio cascade;
drop table if exists fabricante cascade;
drop table if exists tarifa cascade;
drop type if exists t_endereco cascade;

-- Criação de tabelas com as respectivas chaves primárias, restrições de unicidade e checagem.

create table veiculo (
nu_placa		char(7)		not null,
vl_ano_fabricacao	smallint 	not null,
vl_km_atual		int		not null,
dt_aquisicao		timestamp	not null,
cd_modelo		integer		not null,
cd_grupo		integer		    null,
constraint pk_veiculo
	primary key (nu_placa),
constraint ch_veiculo_ano_fabricacao
	check (vl_ano_fabricacao > 2000));

create table modelo (
cd			serial 		not null,    --int , create sequence, set default
nm			varchar(30)	not null,
nu_cnpj_fabricante	char(14)	not null,
constraint pk_modelo
	primary key (cd),
constraint un_modelo_nm
	unique (nm));

create table grupo (
cd			serial 		not null,
nm			varchar(30)	not null,
constraint pk_grupo
	primary key (cd),
constraint un_grupo_nm
	unique (nm));

create table acessorio (
sg			char(3)		not null,
nm			varchar(30)	not null,
constraint pk_acessorio
	primary key (sg),
constraint un_acessorio_sg
	unique (sg));

create table veiculo_acessorio (
nu_placa_veiculo	char(7)		not null,
sg_acessorio		char(3)		not null,
constraint pk_veiculo_acessorio
	primary key (nu_placa_veiculo,sg_acessorio));

create type t_endereco as (
nm_logradouro		varchar(100),
nu			varchar(10),
nm_bairro		varchar(100),
ds_complemento		varchar(200));
	
create table fabricante (
nu_cnpj			char(14)	not null,
nm			varchar(40)	not null,
nu_telefone		varchar(15)	not null,
endereco		t_endereco	not null,
constraint pk_fabricante
	primary key (nu_cnpj));

create table tarifa (
cd_grupo		int		not null,
dt_inicio_vigencia	date		not null,			
vl			numeric(10,2)	not null,
constraint pk_tarifa
	primary key (cd_grupo, dt_inicio_vigencia));

-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo(cd),
	add constraint fk_veiculo_grupo
		foreign key (cd_grupo)
		references grupo(cd)
		on delete set null;
		
alter table modelo
	add constraint fk_modelo_fabricante
		foreign key (nu_cnpj_fabricante)
		references fabricante(nu_cnpj)
		on update cascade;

alter table veiculo_acessorio 
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (nu_placa_veiculo)
		references veiculo (nu_placa)
		on update cascade
		on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sg_acessorio)
		references acessorio (sg)
		on update cascade;

alter table tarifa
	add constraint fk_tarifa_grupo
		foreign key (cd_grupo)
		references grupo(cd);

-- Inserção, alteração, deleção e consulta


insert into grupo (nm)
values 
('Básico'),
('Utilitários'),
('Luxo');

select * 
from grupo;

delete 
from grupo
where nm = 'Utilitários';

select * 
from grupo;

insert into grupo (nm)
values 
('Utilitário');

select * 
from grupo;

insert into tarifa (cd_grupo, dt_inicio_vigencia, vl)
values
((select cd from grupo where nm = 'Básico'),'2020-01-05',50),
((select cd from grupo where nm = 'Utilitário'),'2020-01-05',150),
((select cd from grupo where nm = 'Luxo'),'2020-01-05',250),
((select cd from grupo where nm = 'Básico'),'2020-06-02',60),
((select cd from grupo where nm = 'Utilitário'),'2020-06-02',160);

update tarifa
set dt_inicio_vigencia = '2020-06-04',
    vl = vl + 10
where vl = 160;

select * 
from tarifa;
	
insert into fabricante (nu_cnpj, nm, nu_telefone, endereco)
values
( '45645', 'Caju', '123123123', ('Aguiar','467','Pituba','Nova informação') ),
( '12345678000A00', 'Manga', '45656456456', ('Aguiar','345','Pituba','Outra informação') ),
( '12345678000100', 'UCSal', '123123123', ('Pinto de Aguiar','2355','Pituaçu','Uma coisa é uma coisa e outra coisa é outra coisa') );

select *
from fabricante;

select (endereco).nm_bairro
from fabricante;

-- Listar veículos: placa e idade
select	nu_placa as "Placa",
	date_part('year', current_date) - vl_ano_fabricacao as "Idade"
from 	veiculo;

-- 2. Listar veículos: todos os atributos
-- Filtro: fabricados antes de 2015
select	*
from 	veiculo
where	vl_ano_fabricacao < 2015;

-- 3. Listar veículos: todos os atributos
-- Filtro: adquiridos antes de 2015 e com quilometragem maior que 40000
select	*
from 	veiculo
where	vl_ano_fabricacao < 2015 
and	vl_km_atual > 40000;

-- 4. Listar modelos: todos os atributos
-- Filtro: nome contem a palavra "gol" em qualquer local
select	 * 
from 	modelo
where	upper(nm) like '%GOL%';

select	 * 
from 	modelo
where	nm ilike '%gol%';

-- 5. Listar fabricante:
-- Campos: composição do endereço; na composição de endereço (logradouro todo em
-- minúsculo, primeiros 10 caracteres do complemento); nome do fabricante todo em
-- maiúsculo;
-- Filtro: cnpj com menos de 14 caracteres (lembre-se de remover os caracteres em
-- branco do início e fim do campo durante a consulta) ou que possuam caracteres não
-- numéricos
select  lower((endereco).nm_logradouro) || '-' || (endereco).nu || '-' ||( endereco).nm_bairro || '-' || substr((endereco).ds_complemento,1,10) as "Endereço", 
	upper(nm) as "Nome"
from fabricante
where length(trim(nu_cnpj)) < 14 
or nu_cnpj ~ '[^\d]';

select  concat(lower((endereco).nm_logradouro), '-', (endereco).nu, '-', ( endereco).nm_bairro, '-', substr((endereco).ds_complemento,1,10)) as "Endereço", 
	upper(nm) as "Nome"
from fabricante
where length(trim(nu_cnpj)) < 14 
or nu_cnpj ~ '[^\d]';

-- 6. Listar veículos:
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilômetros rodados por todos os
-- veículos
select	count(*),
	max(vl_km_atual),
	min(vl_km_atual),
	avg(vl_km_atual),
	sum(vl_km_atual)
from veiculo;

-- 7. Listar veículos:
-- Campos: ano de fabricação
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilometros rodados por todos os
-- veículos
select	vl_ano_fabricacao,
	count(*),
	max(vl_km_atual),
	min(vl_km_atual),
	avg(vl_km_atual),
	sum(vl_km_atual)
from veiculo
group by vl_ano_fabricacao;

-- Uso da função position
select position('Sal' in nm)
from fabricante;


