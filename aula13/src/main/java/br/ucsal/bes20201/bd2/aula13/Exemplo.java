package br.ucsal.bes20201.bd2.aula13;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Aluno aluno = new Aluno(10, "Claudio");

		em.persist(aluno);

		em.getTransaction().commit();

		em.detach(aluno);

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		aluno = em.find(Aluno.class, 10);

		em.detach(aluno);

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		aluno = em.merge(aluno);

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		em.getTransaction().begin();

		aluno.setNome("Maria da Silva");

		em.getTransaction().commit();

		em.getTransaction().begin();
		
		aluno.setNome("Cajuina");

		em.refresh(aluno);

		System.out.println("aluno após refresh=" + aluno);

		em.remove(aluno);
		
		em.getTransaction().commit();

		emf.close();

	}

}
