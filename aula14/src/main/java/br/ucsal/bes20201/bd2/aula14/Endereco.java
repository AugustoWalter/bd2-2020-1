package br.ucsal.bes20201.bd2.aula14;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Endereco {

	private String rua;

	private String bairro;

	private String numero;

	private String complemento;

	@ManyToOne
	private Cidade cidade;

	public Endereco() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Endereco(String rua, String bairro, String numero, String complemento) {
		super();
		this.rua = rua;
		this.bairro = bairro;
		this.numero = numero;
		this.complemento = complemento;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
