package br.ucsal.bes20201.bd2.aula14;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
//@Table(name = "aln_aluno", schema = "aula")
@Table(name = "tab_aluno", uniqueConstraints = {
		@UniqueConstraint(name = "un_aluno_rg", columnNames = { "rg", "orgaoExpedidorRg", "ufRg" }) })
@SequenceGenerator(name = "sq_aluno", sequenceName = "seq_aluno")
public class Aluno {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_aluno")
	private Integer matricula;

	@Column(unique = true)
	private String nome;

	private String rg;

	private String orgaoExpedidorRg;

	private String ufRg;

	@Embedded
	private Endereco endereco;

	@ManyToMany
	private List<Disciplina> disciplinas;

	@Transient
	private Boolean selected;

	public Aluno() {
	}

	public Aluno(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoExpedidorRg() {
		return orgaoExpedidorRg;
	}

	public void setOrgaoExpedidorRg(String orgaoExpedidorRg) {
		this.orgaoExpedidorRg = orgaoExpedidorRg;
	}

	public String getUfRg() {
		return ufRg;
	}

	public void setUfRg(String ufRg) {
		this.ufRg = ufRg;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", rg=" + rg + ", orgaoExpedidorRg="
				+ orgaoExpedidorRg + ", ufRg=" + ufRg + ", endereco=" + endereco + "]";
	}

}
