package br.ucsal.bes20201.bd2.aula14;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Estado {

	@Id
	private String sigla;

	private String nome;

	@OneToMany(mappedBy = "estado")
	private List<Cidade> cidades;

	public Estado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Estado(String sigla, String nome, List<Cidade> cidades) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.cidades = cidades;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

}
