﻿CREATE OR REPLACE FUNCTION increment (i integer) 
RETURNS integer 
AS 
$$
BEGIN

           RETURN i + 1;

END;
$$
LANGUAGE plpgsql;

create table despesa (
ds	varchar(100) 	not null,
vl	integer		not null);

insert into despesa (ds, vl)
values 
('agua',300),
('energia', 400);


select increment(vl)
from despesa;

select increment(10);



-- Cenário (final anos 90, início dos anos 2000):

-- 1 milhão de registros financeiros em uma tabela;
-- Um algoritmo "complexo", que envolve dados de diversas tabelas a serem consolidadas junto aos 1 milhão;
-- Máquina de usuário com baixo poder de processamento;
-- Redes com baixa capacidade para transitar informações;
-- Não tinha muitas clientes cliente conectadas a um servidor;
-- Arquitetura Cliente x Servidor (geralmente era o SGBD);



-- Se vc implementasse uma function, vantagens:
-- O algoritmo executaria no SGBD (máquina melhor);
-- Apenas o resultado do processamento iria para o cliente (reduziria o tráfego na rede, reduziria a demanda de processamento na máquina do cliente);



-- Cenário atual:

-- Continua com muitos dados, aumentou bastantes os dados;
-- Novo elemento de processamento: servidor de aplicação;
-- Separação significativa entre armazenamento/recuperação de dados x processamento do dado;
-- Redes estão extremamente rápidas (GigaBit);
-- Servidor(es) de aplicação físicamente próximo ao SGBD. Muitas vezes com canal de rede "ponto a ponto";
-- Proporção maior de clientes em relação aos SGBDs;
-- Proporção menor (escalar) de clientes em relação aos servidores de aplicação;
-- Linguagem de programação para escrever functions ela é específica;


-- Diferença entre fuction e stored procedure: stored procedures é uma function com retorno void;




-- Trigger - motivação:
-- Não deveríamos ter dados redundantes num banco de dados - faço para evitar inconsistência nos dados;


create table conta_corrente (
	nu		serial		not null,
	nu_cpf		char(11)	not null,
	nm		varchar(100)	not null,
	constraint pk_conta_corrente
		primary key (nu));

create table movimento (
	nu_seq		serial		not null,
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(200)	    null,
	constraint pk_movimento
		primary key (nu_seq);

create table saldo
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	contraint pk_saldo
		primary key (nu_conta, dt));

alter table movimento
	add constraint fk_movimento_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;

alter table saldo
	add constraint fk_saldo_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;

		
-- Problema: emissão do extrato para um mês/ano específico de um cliente que possui conta corrente à 15 anos;

insert into conta_corrente (nu_cpf, nm) 
values 
('123','Claudio');

insert into movimento (nu_conta, dt, tp, ds_historico)
values 
-- 40.000 movimentos antes de 2020-01-05... registros de 15 anos atrás!!!!
((select nu from conta_corrente where nu_cpf='123'), '2020-01-05',  500, 'Depósito'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-08', -200, 'Pagamento de conta - Luz'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-10', -100, 'Saque'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-03', 2000, 'Salário'),

((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -900, 'Transferência'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -150, 'Pagamento de conta - Telefone'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-08', -400, 'Saque');

-- Saldo inicial = 0
--	nu_conta	Data	Saldo
--		1	05/01	  500
--		1	08/01	  300
--		1	10/01	  200
--		1	03/02	2.200
--		1	06/02	1.150
--		1	08/02	  750

-- Consulta que retorna o último saldo disponível para o correntista de número 1 para a data de referência 05/02/2020.
select	s.vl
from	saldo s
where	s.nu_conta	= 1
and	s.dt		= (   select max(s2.dt)
				from s2.saldo
			       where s2.nu_conta = 1
			       and   s2.dt < '2020-02-05');

-- Extrato do dia 05/02 ao dia 05/03 para o correntista de número de conta 1. 
--	Saldo inicial:  				 2.200
--	06/02	Transferência  				-  900
--	06/02	Pagamento de conta - Telefone		-  150
--	08/02	Saque				 	-  400
-- 	Saldo final:					   750

-- Extrato do dia 11/01 ao dia 03/02. Saldo inicial:   200



-- Cenário 1:
-- O servidor de aplicação para cada movimento inserido/excluído/alterado PELA aplicação deve calcular o saldo e armazená-lo na base;
-- Problemas:
-- Programador novato pode fazer uma implementação errada no cálculo do saldo;
-- Programador novato pode esquecer de executar a rotina de cálculo de saldo para os movimentos atualizados pela aplicação;
-- Alguém pode atualizar a base diretamente, sem passar pela camada de aplicação;
-- Vantagens:
-- A linguagem utilizada pelo servidor de aplicação é de domínio da equipe de desenvolvimento;
-- As ferramentas de debug para as liguagens utilizadas nos servidores de aplicação, são ferramentas bastante robustas, ricas em recursos;


-- Cenário 2:
-- O SGBD, a partir das operações feitas na tabela de movimento (insert, update, delete), atualiza automaticamente o valor do saldo - TRIGGER;
-- Vantagens:
-- Independe de onde se originou a atualização (diretamente na base de dados ou através do servidor de aplicação), o saldo sempre será atualizado;
-- Quem implementa a rotina de atualização tem domínio do SGBD, geralmente não é um profissional novato;
-- Devantagem:
-- A tecnologia (linguagem) geralmente não é a mesma dos servidores de aplicação;
-- As ferramentas de debug dentro do SGBD são inexistens ou muito pobres;
-- O comportamento do trigger é transparente e qualquer falha na implementação não fica tão visível quanto nas aplicações, principalmente pela falta de ferramentas de automação de testes;


-- DESAFIOS:

-- 1. Criar um trigger que, a partir da inserção (INSERT) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

-- 2. Criar um trigger que, a partir da remoção (DELETE) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

-- 3. Criar um trigger que, a partir da atualização (UPDATE) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

-- 4. Criar um mecanismo de AUDITORIA para modificações (INSERT, UPDATE, DELETE) de dados na tabela de MOVIMENTO
-- 4.1. Estrutura de dados para registro da auditoria;
-- 4.2. Triggers para alimentar a estrutura de dados;










